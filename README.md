# Customer API Specification

##Overview

The Customer API is used to get customer data with optional fields as well as all data. Its also used to create, update and delete customers.

##Installation

Import the project into a Mule IDE. To run it right click on the project and click on Run As -> Mule Application

On the browser go to http://127.0.0.1:8081/console/ which will show the Customer API
The API is self describing.

##Index

Endpoint  | Description | Optional Query String
------------- | ------------- | -------------
GET /customers  | Get Customers | First Name, Last Name. Use field string to supply what fields u want returned in response. eg . GET /customers?fields=firstName,lastName will only return first name and last name in response
POST/customers  | Create Customers
GET /customers/{id}  | Get Customer using Id
PUT /customers/{id}  | Update Customers using Id
DELETE /customers/{id}  | Delete Customers using Id


## License
Customer API is openly developed and built under the MIT License.

## Current Version
The current version of the Customer API specification is v1.